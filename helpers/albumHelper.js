
var albumHelper = {};
var User = require('../models/user');
var Album = require('../models/album');
var handleErrorResponse = require('../helpers/errorHelper').handleErrorResponse;
var utils = require('./utils');

albumHelper.getSingleAlbum = function (req, res, next) {
    var possibleErrors = [];
    var subscribedUsers = [];

    Album.findOne({ _id: req.params.id }).populate("albumUserCreator").exec(function (err, album) {
        if (err) {
            possibleErrors.push('Unable to find album for given ids');
        }
        if (possibleErrors.length > 0) {
            return handleErrorResponse(possibleErrors, res);
        }
        else {
            req.singleAlbum = album;
            next();
        }
    });
}

albumHelper.getAlbums = function (req, res, next) {
    var possibleErrors = [];
    var subscribedUsers = [];

    Album.find({ stage: req.params.stage }, function (err, albums) {
        if (err) {
            possibleErrors.push('Unable to find album for given ids');
        }
        if (possibleErrors.length > 0) {
            return handleErrorResponse(possibleErrors, res);
        }
        else {
            req.albums = albums;
            next();
        }
    });
}


albumHelper.getBoughtAlbums = function (req, res, next) {
    var possibleErrors = [];
    User.findOne({ _id: req.user._id }).populate({path:'bought.album', populate:{ path: 'content', populate: { path: 'image' } }}).exec(function (err, user) {

        if (err || !user || user.deleted) {
            possibleErrors.push('Unable to find user for given id');
        }

        if (possibleErrors.length > 0) {
            return handleErrorResponse(possibleErrors, res);
        } else {

            var allBought = user.bought;
            var albums = allBought.map(function (x) { return x.album; });

            if (err || !albums) {
                albums = [];
            }

            req.boughtAlbums = albums;
            next();
        }
    });
}

albumHelper.getSubscribedAlbums = function (req, res, next) {
    var possibleErrors = [];
    var subscribedUsers = [];
    //TODO: Add check date
    req.user.subscribedTo.forEach(function (sub) {
        subscribedUsers.push(sub.user);
    }, this);

    Album.find({ albumUserCreator: { $in: subscribedUsers }, stage: 'published' }).populate({ path: 'content', populate: { path: 'image' } }).exec(function (err, albums) {
        if (err) {
            possibleErrors.push('Unable to find album for given ids');
        }
        if (possibleErrors.length > 0) {
            return handleErrorResponse(possibleErrors, res);
        }
        else {
            req.subscribedAlbums = albums;
            next();
        }
    });
}

albumHelper.getFreeAlbums = function (req, res, next) {
    var possibleErrors = [];

    Album.find({ price: 0, albumUserCreator: { $in: utils.convertToIds(req.following, 'userId') }, stage: 'published' }).populate({ path: 'content', populate: { path: 'image' } }).exec(function (err, albums) {
        if (err) {
            possibleErrors.push('Unable to find album for given ids');
        }
        if (possibleErrors.length > 0) {
            return handleErrorResponse(possibleErrors, res);
        }
        else {
            req.freeAlbums = albums;
            next();
        }
    });
}

module.exports = albumHelper;