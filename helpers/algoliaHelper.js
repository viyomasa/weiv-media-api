var algoliaHelper = {}
var algoliasearch = require('algoliasearch');
var client = algoliasearch("GLNG8ESMBL", "739ec341d50c9c50685f97f5e3535c5a");
var albumIndex = client.initIndex('AlbumIndex');
var userIndex = client.initIndex('UserIndex');
var User = require('../models/user');
var Album = require('../models/album');

algoliaHelper.syncUsers = function () {
    User.find({confirmed:true,is_content_creator:true}, function (err, users) {
        var resultUser = [];
        users.forEach(function (doc) {
            doc = doc.toObject()
            var suid = doc._id.toString();
            doc.objectID = suid;
            resultUser.push(doc);
        }, this);
        userIndex.saveObjects(resultUser, null);
    });
}
algoliaHelper.syncAlbums = function () {
    Album.find({ stage:'published' }).populate({path:'content',populate:{path:'image'}}).exec(function (err, albums) {
        var resultAlbum = [];
        albums.forEach(function (doc) {
            doc = doc.toObject()
            var suid = doc._id.toString();
            doc.objectID = suid;
            resultAlbum.push(doc);
        }, this);
        albumIndex.saveObjects(resultAlbum, null);
    });
}




module.exports = algoliaHelper;