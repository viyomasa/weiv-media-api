'use strict';
var AWS = require('aws-sdk');

var s3 = new AWS.S3({
    apiVersion: '2012–09–25'
});

var eltr = new AWS.ElasticTranscoder({
    apiVersion: '2012–09–25',
    region: 'us-west-1'
});
exports.handler = function(event, context) {
    console.log('Executing Elastic Transcoder Orchestrator');
    var bucket = event.Records[0].s3.bucket.name;
    var key = event.Records[0].s3.object.key;
    var pipelineId = '1497976605668-ewi2bf';
    if (bucket !== 'serverlessimageresize-imagebucket-1pi041yp35m1v') {
        context.fail('Incorrect Video Input Bucket');
        return;
    }
    var srcKey = decodeURIComponent(event.Records[0].s3.object.key.replace(/\+/g, " ")); //the object may have spaces
    var newKey = key.split('.')[0];
    var params = {
        PipelineId: pipelineId,
        //OutputKeyPrefix: newKey + '/',
        Input: {
            Key: srcKey,
            FrameRate: 'auto',
            Resolution: 'auto',
            AspectRatio: 'auto',
            Interlaced: 'auto',
            Container: 'auto'
        },
        Outputs: [{
            Key: newKey + '.mp4',
            ThumbnailPattern: newKey + '-{count}',
            PresetId: '1351620000001-000020',
            Watermarks: [{
                InputKey: 'watermarks/video-placeholder.png',
                PresetWatermarkId: 'BottomRight'
            }]
        }]
    };
    console.log('Starting Job');
    eltr.createJob(params, function(err, data){
        if (err){
            console.log(err);
        } else {
            console.log(data);
        }
        context.succeed('Job well done');
    });
};