/**
 * Created by nemanja on 4/2/17.
 */


var mongoose = require('mongoose');
var algoliasearch = require('algoliasearch');
var client = algoliasearch("GLNG8ESMBL", "739ec341d50c9c50685f97f5e3535c5a");
var Followed = require('../models/followed');
var BasePostObjectSchema = require('../models/postobjects/basePostObject');
var index = client.initIndex('AlbumIndex');

var Schema = mongoose.Schema;

var AlbumSchema = new Schema({
    albumUserCreator: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    description: { type: String },
    name: { type: String },
    created_on: { type: Date, default: Date.now() },
    isPaid: { type: Boolean, default: false },
    isPublished: { type: Boolean, default: false },
    isFeatured: { type: Boolean, default: false },
    isDiscoverable: { type: Boolean, default: false },
    price: { type: Number },
    tags: [{ type: String }],
    totalPurchased: { type: Number },
    userRating: [{
        rating: { type: Number },
        user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' }
    }],
    boughtBy: [{
        user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
        transaction: { type: String },
        date: { type: Date }
    }],
    customNotification: { type: String },
    metadata: {
        backgroundColor: { type: String },
    },
    content: [{ type: mongoose.Schema.Types.ObjectId, ref: 'BasePostObject' }],
    stage: { type: String }
});

AlbumSchema.path('price').get(function (num) {
    console.log(num.toFixed(2));
    return num.toFixed(2);
});

AlbumSchema.virtual('rating').get(function () {
    var total = 0;
    for (var i = 0; i < this.userRating.length; i++) {
        total += this.userRating[i].rating || 0;
    }
    return total / this.userRating.length;
});

AlbumSchema.virtual('salesCount').get(function () {
    return this.boughtBy.length;
});

AlbumSchema.pre('save', function (next) {
    this.wasNew = this.isNew;
    next();
});

AlbumSchema.post('save', function (doc) {
    if (doc.stage == "published") {
        doc.populate({ path: 'content', populate: { path: 'image' } }, function (err) {
            index.addObject(doc, doc._id.toString(), function (err, content) {
                console.log(content);
            });
        });
    }
    else if (doc.stage == "published-deleted") {
                index.deleteObject(doc._id.toString(), function (err) {
                    if (!err) {
                      console.log("deleted");
                    }
                });
            }
});

AlbumSchema.post('update', function () {
    var self = this;
    var uid = self._conditions._id;
    self.findOne({ _id: uid }, function (err, doc) {
        if (err)
            console.log(err);
        else {
            var suid = doc._id.toString();
            doc.objectID = suid;
            if (doc.stage == "published") {
                index.saveObject(doc, doc._id.toString(), function (err, content) {
                    console.log(content);
                });
            }
            else if (doc.stage == "published-deleted") {
                index.deleteObject(suid, function (err) {
                    if (!err) {
                      console.log("deleted");
                    }
                });
            }
        }
    });

});

AlbumSchema.methods.outputMappedResult = function (images) {
    var $this = this;
    var outputImages = [];
    if (images) {
        images.forEach(function (image) {
            if (image.albumId == $this._id) {
                outputImages.push(image.outputMappedResult());
            }
        });
    }

    return {
        albumId: $this._id,
        salesCount: $this.salesCount,
        description: $this.description,
        name: $this.name,
        isPaid: $this.isPaid,
        isPublished: $this.isPublished,
        isFeatured: $this.isFeatured,
        isDiscoverable: $this.isDiscoverable,
        price: $this.price,
        tags: $this.tags,
        totalPurchased: $this.totalPurchased,
        rating: $this.rating,
        images: outputImages,
        metadata: $this.metadata
    };
};

AlbumSchema.methods.outputMappedResultWithCreator = function (images, creators, following) {
    var $this = this;
    var followingUsers = [];
    var albumCreator = {};

    var outputImages = [];

    if (images) {
        images.forEach(function (image) {
            if (image.albumId == $this._id) {
                outputImages.push(image.outputMappedResult());
            }
        });
    }
    if (creators) {
        creators.forEach(function (creator) {
            var creatorid = "";
            var albumcreatorid = "";
            creatorid = creator._id;
            albumcreatorid = $this.albumUserCreator;
            if (creatorid.equals(albumcreatorid)) {
                creatorJson = creator.toJSON();
                if (following != null) {
                    var isfollowing = following.some(function (fuser) {
                        return fuser.userId.equals(creatorid);
                    });
                    creatorJson.following = isfollowing;
                }
                albumCreator = creatorJson;
            }
        });
    }

    var response = {
        albumUserCreator: albumCreator,
        albumId: $this._id,
        stage:$this.stage,
        salesCount: $this.salesCount,
        _id: $this._id,
        description: $this.description,
        name: $this.name,
        isPaid: $this.isPaid,
        isPublished: $this.isPublished,
        price: $this.price,
        tags: $this.tags,
        isFeatured: $this.isFeatured,
        isDiscoverable: $this.isDiscoverable,
        totalPurchased: $this.totalPurchased,
        created_on: $this.created_on,
        rating: $this.rating,
        images: outputImages,
        coverImage: outputImages.length > 0 ? outputImages[0].imageThumbUrl300 : ''
    }
    return response;

};

module.exports = mongoose.model('Album', AlbumSchema);
