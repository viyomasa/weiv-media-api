/**
 * Created by nemanja on 6/3/17.
 */
var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var ChatSchema = new Schema({
    userSender: {
        user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' }
    },
    userReceiver: {
        user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' }
    },
    created_on: { type: Date, default: Date.now() },
    isEvenMessage: { type: Boolean, default: false },
    message: { type: String },
    isRead: { type: Boolean, default: false }
});

ChatSchema.methods.outputMappedResult = function() {
    var $this = this;

    return {
        _id: $this._id,
        userSender: $this.userSender.user._id,
        userReceiver: $this.userReceiver.user._id,
        isEvenMessage: $this.isEvenMessage,
        message: $this.message,
        isRead: $this.isRead,
        created_on: $this.created_on
    };
};

module.exports = mongoose.model('Chat', ChatSchema);