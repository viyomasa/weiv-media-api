var mongoose = require('mongoose');
var User= require('./user');
var Schema = mongoose.Schema;

var FollowingSchema = new Schema({
    userId: { type: String, trim: true, required: false, lowercase: true },
    users:[{
        userId:{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }
    }]
});

FollowingSchema.static('findOrCreate', function (profile, callback) {
    var self = this;
    this.findOne({userId: profile._id},
        function (err, following) {
            if (err) return callback(err);
            if (following)
            {
                return callback(null, following);
            }
            following = new self({userId:profile._id,users:[]});
            following.save(callback);
        });
});

module.exports = mongoose.model('Following', FollowingSchema);