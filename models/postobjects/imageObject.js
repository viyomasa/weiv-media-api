/**
 * Created by nemanja on 6/3/17.
 */
var mongoose = require('mongoose');
var BasePostObject=require('./basePostObject');

var Schema = mongoose.Schema;

var ImageObjectSchema = new Schema({
   image: { type: mongoose.Schema.Types.ObjectId, ref: 'Image' },
   dimension:{type:String}
});

ImageObjectSchema.methods.outputMappedResult = function() {
    var $this = this;
    return {
        type:$this.postType
    };
};

module.exports = BasePostObject.discriminator('ImageObject', ImageObjectSchema);