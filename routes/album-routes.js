/**
 * Created by nemanja on 4/2/17.
 */

var album_routes = require('express').Router();
var config = require('../config');
var User = require('../models/user');
var Image = require('../models/image');
var Album = require('../models/album');
var Activity = require('../models/activity');
var Order = require('../models/order');
var auth = require("../customAuth")();
var mongoose = require('mongoose');
var stripe = require("stripe")("sk_test_HUgR0fijvynpgp1EmKz6Z152");
var handleErrorResponse = require('../helpers/errorHelper').handleErrorResponse;
var followingHelper = require('../helpers/followingHelper');
var albumHelper = require('../helpers/albumHelper');
var imageHelper = require('../helpers/imageHelper');
var notificationsHelper = require('../helpers/notificationsHelper');
var activityHelper = require('../helpers/activityHelper');
var postObjectsHelper = require('../helpers/postObjectsHelper');


album_routes.post('/', auth.authenticate(), function (req, res) {
    var possibleErrors = [];
    var newAlbum = new Album();
    newAlbum.description = req.body.description;
    newAlbum.tags = req.body.tags;
    newAlbum.name = req.body.name;
    newAlbum.price = req.body.price;
    newAlbum.isPaid = req.body.isPaid;
    newAlbum.isPublished = req.body.isPublished;
    newAlbum.albumUserCreator = req.user._id;
    newAlbum.created_on = Date.now();
    newAlbum.totalPurchased = 0;


    newAlbum.save(function (err) {
        if (err) {
            possibleErrors.push('Album not saved!');
        }

        if (possibleErrors.length == 0) {
            var output = newAlbum.outputMappedResult();
            output.status = 'created';
            return res.status(201).jsonp(output);
        } else {
            return handleErrorResponse(possibleErrors, res);
        }
    });
});


album_routes.post('/v2/', auth.authenticate(), function (req, res) {
    var possibleErrors = [];
    var newAlbum = new Album();
    newAlbum.description = req.body.description;
    newAlbum.tags = req.body.tags;
    newAlbum.name = req.body.name;
    newAlbum.price = req.body.price;
    newAlbum.isPaid = req.body.isPaid;
    newAlbum.isPublished = req.body.isPublished;
    newAlbum.albumUserCreator = req.user._id;
    newAlbum.created_on = Date.now();
    newAlbum.totalPurchased = 0;
    newAlbum.stage = "created";
    newAlbum.metadata = {};
    newAlbum.metadata = req.body.metadata;
    newAlbum.customNotification = req.body.customNotification;


    newAlbum.save(function (err) {
        if (err) {
            possibleErrors.push('Album not saved!');
        }

        if (possibleErrors.length == 0) {
            var output = newAlbum.outputMappedResult();
            output.status = 'created';
            return res.status(201).jsonp(output);
        } else {
            return handleErrorResponse(possibleErrors, res);
        }
    });
});

album_routes.put('/v2/:id', auth.authenticate(), function (req, res) {
    var possibleErrors = [];

    if (!req.params.id || req.params.id == 0) {
        possibleErrors.push('id is required!');
    }
    if (possibleErrors.length > 0) {
        return handleErrorResponse(possibleErrors, res);
    }


    Album.findOne({ _id: req.params.id }, function (err, album) {
        if (err || !album) {
            possibleErrors.push('Unable to find album for given id!');
        }

        Album.update({ _id: req.params.id }, req.body, function (err) {
            if (err) {
                possibleErrors.push('Album not saved!');
            }

            if (possibleErrors.length == 0) {


                return res.status(200).jsonp({ status: 'updated' });
            } else {
                return handleErrorResponse(possibleErrors, res);
            }
        });
    });
});

album_routes.patch('/v2/:id', auth.authenticate(), albumHelper.getSingleAlbum, function (req, res) {
    var possibleErrors = [];
    var operations = req.body.operations;
    var operationCount = { operationsLeft: operations.length };
    var operationFinished = (function () {
        this.operationsLeft--;
        if (this.operationsLeft == 0) {
            Album.update({ _id: req.params.id }, req.singleAlbum, function (err) {
                if (err) {
                    possibleErrors.push('Album not saved!');
                }
                if (possibleErrors.length == 0) {
                    return res.status(200).jsonp({ status: 'updated' });
                } else {
                    return handleErrorResponse(possibleErrors, res);
                }
            });  
        }
    }).bind(operationCount);


    operations.forEach(function (operation) {
        var op = operation.op;
        var postObject = operation.postObject;
        switch (op) {
            case "add":
                if (postObject._id != null && postObject._id != "")
                    possibleErrors.push("You are adding new object with id.");
                else {
                    postObjectsHelper.generatePostObject(postObject, function (po) {
                        req.singleAlbum.content.push(po);
                        operationFinished()
                    });
                }
                break;
            case "changeOrder":
                if (postObject._id == null)
                    possibleErrors.push("You are moving object without id.");
                else {
                    req.singleAlbum.content = req.singleAlbum.content.filter(function (obj) {
                        return obj._id !== postObject._id;
                    });
                    operationFinished();
                }
                break;
            case "remove":
                if (postObject._id == null)
                    possibleErrors.push("You are moving object without id.");
                else {
                    req.singleAlbum.content = req.singleAlbum.content.filter(function (obj) {
                        return !obj.equals(postObject._id);
                    });
                    operationFinished();
                }
                break;
            default:
                break;
        }
    }, this);

});

album_routes.post('/v2/draft/:id', albumHelper.getSingleAlbum, auth.authenticate(), function (req, res) {
    req.singleAlbum.stage = "draft";
    req.singleAlbum.save(function (err) {
        return res.status(200).jsonp(req.singleAlbum);
    });
});

album_routes.post('/v2/publish/:id', albumHelper.getSingleAlbum, auth.authenticate(), followingHelper.getFollowingUsersWithDetails, function (req, res) {
    req.singleAlbum.stage = "published";
    req.singleAlbum.save(function (err) {
        var allFCMs=[];
        req.following.forEach(function(elFollowing) {
            allFCMs=allFCMs.concat(elFollowing.fcmList);
        }, this);
        if(allFCMs.length>0)
        {
            if(!req.singleAlbum.customNotification || req.singleAlbum.customNotification=="")
            notificationsHelper.sendNotifications(req.user.username + " has published new album.", req.user.username + " has published new album. ", allFCMs);
            else
            notificationsHelper.sendNotifications(req.singleAlbum.customNotification, req.singleAlbum.customNotification, allFCMs);
        }
        
        return res.status(200).jsonp(req.singleAlbum);
    });
});

album_routes.delete('/v2/my/draft/:id', albumHelper.getSingleAlbum, auth.authenticate(), function (req, res) {
    req.singleAlbum.stage = "draft-deleted";
    req.singleAlbum.save(function (err) {
        return res.status(200).jsonp(req.singleAlbum);
    });
});

album_routes.delete('/v2/my/publish/:id', albumHelper.getSingleAlbum, auth.authenticate(), function (req, res) {
    req.singleAlbum.stage = "published-deleted";
    req.singleAlbum.save(function (err) {
        return res.status(200).jsonp(req.singleAlbum);
    });
});






album_routes.put('/:id', auth.authenticate(), function (req, res) {
    var possibleErrors = [];

    if (!req.params.id || req.params.id == 0) {
        possibleErrors.push('id is required!');
    }
    if (possibleErrors.length > 0) {
        return handleErrorResponse(possibleErrors, res);
    }

    if (req.body.images) {
        var tmpArray = [];
        if (Object.prototype.toString.call(req.body.images) === '[object Array]') {
            tmpArray = req.body.images;
            req.body.images = undefined;

            // Update Images for album
            tmpArray.forEach(function (image) {
                Image.update({ _id: image.imageId }, image, function (errImg) {
                    if (errImg) {
                        possibleErrors.push('Unable to update image for given imageId: ' + image.imageId);
                    }
                });
            });
        }
    }


    Album.findOne({ _id: req.params.id }, function (err, album) {
        if (err || !album) {
            possibleErrors.push('Unable to find album for given id!');
        }


        Album.update({ _id: req.params.id }, req.body, function (err) {
            if (err) {
                possibleErrors.push('Album not saved!');
            }

            if (possibleErrors.length == 0) {


                return res.status(200).jsonp({ status: 'updated' });
            } else {
                return handleErrorResponse(possibleErrors, res);
            }
        });
    });
});

album_routes.delete('/:id', auth.authenticate(), function (req, res) {
    var possibleErrors = [];
    if (!req.params.id || req.params.id == 0) {
        possibleErrors.push('id is required!');
    }
    if (possibleErrors.length > 0) {
        return handleErrorResponse(possibleErrors, res);
    }

    Album.findOne({ _id: req.params.id }, function (err, album) {
        if (err || !album) {
            possibleErrors.push('Unable to find album for given id!');
        }

        if (!album || !album.albumUserCreator.equals(req.user._id) || album.isPublished) { //req.user._id) {
            possibleErrors.push('Not possible to delete this album!');
        }

        if (possibleErrors.length == 0) {
            album.remove(function (err) {
                if (err) {
                    possibleErrors.push('Unable to remove album');
                }

                if (possibleErrors.length == 0) {
                    return res.status(204).jsonp();
                } else {
                    return handleErrorResponse(possibleErrors, res);
                }
            });
        } else {
            return handleErrorResponse(possibleErrors, res);
        }
    });
});

album_routes.get('/v2/my/published', auth.authenticate(), function (req, res) {
    var possibleErrors = [];
    var skip = 0;
    var limit = 1000;
    var count = 0;
    if (req.query.size && Number(req.query.size) > 0) {
        limit = Number(req.query.size);
    }
    if (req.query.page && Number(req.query.page) >= 0) {
        skip = Number(req.query.page) * limit;
    }

    if (possibleErrors.length > 0) {
        return handleErrorResponse(possibleErrors, res);
    }

    Album.find({ albumUserCreator: req.user._id, stage: 'published' }).populate({ path: 'content', populate: { path: 'image' } }).sort('-created_on').exec(function (err, albums) {
        if (err || !albums) {
            possibleErrors.push('Unable to find albums for given user!');
        }
        res.status(200).jsonp(albums);
    });
});

album_routes.get('/v2/my/draft', auth.authenticate(), function (req, res) {
    var possibleErrors = [];
    var skip = 0;
    var limit = 1000;
    var count = 0;
    if (req.query.size && Number(req.query.size) > 0) {
        limit = Number(req.query.size);
    }
    if (req.query.page && Number(req.query.page) >= 0) {
        skip = Number(req.query.page) * limit;
    }

    if (possibleErrors.length > 0) {
        return handleErrorResponse(possibleErrors, res);
    }

    Album.find({ albumUserCreator: req.user._id, stage: 'draft' }).populate({ path: 'content', populate: { path: 'image' } }).sort('-created_on').exec(function (err, albums) {
        if (err || !albums) {
            possibleErrors.push('Unable to find albums for given user!');
        }
        res.status(200).jsonp(albums);
    });
});



album_routes.get('/created', auth.authenticate(), function (req, res) {
    var possibleErrors = [];
    var skip = 0;
    var limit = 1000;
    var count = 0;
    if (req.query.size && Number(req.query.size) > 0) {
        limit = Number(req.query.size);
    }
    if (req.query.page && Number(req.query.page) >= 0) {
        skip = Number(req.query.page) * limit;
    }

    if (possibleErrors.length > 0) {
        return handleErrorResponse(possibleErrors, res);
    }

    Album.count({ albumUserCreator: req.user._id }, function (err, numOfAlbums) {
        count = numOfAlbums;
        Album.find({ albumUserCreator: req.user._id }).skip(skip).limit(limit).exec(function (err, albums) {
            if (err || !albums) {
                possibleErrors.push('Unable to find albums for given user!');
            }

            var albumIds = albums.map(function (x) { return x._id; });
            Image.find({ albumId: { $in: albumIds } }, function (errI, images) {

                var albums_output = albums.map(function (x) { return x.outputMappedResult(images); });

                if (possibleErrors.length == 0) {
                    return res.status(200).jsonp({
                        page: skip / limit,
                        size: limit,
                        total: count,
                        albums: albums_output
                    });
                } else {
                    return handleErrorResponse(possibleErrors, res);
                }
            });
        });
    });
});

album_routes.get('/discover', auth.authenticate(), followingHelper.getFollowingUsers, function (req, res) {
    var possibleErrors = [];
    var skip = 0;
    var limit = 1000;
    var count = 0;
    var searcherId = mongoose.Types.ObjectId();
    if (req.query.size && Number(req.query.size) > 0) {
        limit = Number(req.query.size);
    }
    if (req.query.page && Number(req.query.page) >= 0) {
        skip = Number(req.query.page) * limit;
    }

    if (possibleErrors.length > 0) {
        return handleErrorResponse(possibleErrors, res);
    }
    if (req.user) {
        searcherId = req.user._id;
    }

    Album.count({ isDiscoverable: true, albumUserCreator: { $ne: searcherId } }, function (err, numOfAlbums) {
        count = numOfAlbums;
        Album.find({ isDiscoverable: true, albumUserCreator: { $ne: searcherId } }).skip(skip).limit(limit).exec(function (err, albums) {
            if (err || !albums) {
                possibleErrors.push('Unable to find albums that are marked as Discoverable!');
            }

            var albumCreatorIds = albums.map(function (x) { return x.albumUserCreator; });

            User.find({ _id: albumCreatorIds }, function (err, creators) {

                var albumIds = albums.map(function (x) { return x._id; });
                Image.find({ albumId: { $in: albumIds } }, function (errI, images) {

                    var albums_output = albums.map(function (x) { return x.outputMappedResultWithCreator(images, creators, req.following); });

                    if (possibleErrors.length == 0) {
                        return res.status(200).jsonp({
                            page: skip / limit,
                            size: limit,
                            total: count,
                            albums: albums_output
                        });
                    } else {
                        return handleErrorResponse(possibleErrors, res);
                    }
                });
            });
        });
    });
});

album_routes.get('/featured', auth.authenticate(), followingHelper.getFollowingUsers, function (req, res) {
    var possibleErrors = [];
    var skip = 0;
    var limit = 1000;
    var count = 0;
    var searcherId = mongoose.Types.ObjectId();

    if (req.query.size && Number(req.query.size) > 0) {
        limit = Number(req.query.size);
    }
    if (req.query.page && Number(req.query.page) >= 0) {
        skip = Number(req.query.page) * limit;
    }

    if (possibleErrors.length > 0) {
        return handleErrorResponse(possibleErrors, res);
    }

    if (req.user) {
        searcherId = req.user._id;
    }

    Album.count({ isFeatured: true, stage: "published", albumUserCreator: { $ne: searcherId } }, function (err, numOfAlbums) {
        count = numOfAlbums;
        Album.find({ isFeatured: true, stage: "published", albumUserCreator: { $ne: searcherId } }).populate({ path: 'content', populate: { path: 'image' } }).populate("albumUserCreator").sort({ created_on: -1 }).skip(skip).limit(limit).exec(function (err, albums) {
            if (err || !albums) {
                possibleErrors.push('Unable to find albums that are marked as Featured!');
            }
            if (possibleErrors.length == 0) {
                return res.status(200).jsonp({
                    page: skip / limit,
                    size: limit,
                    total: count,
                    albums: albums
                });
            } else {
                return handleErrorResponse(possibleErrors, res);
            }
        });
    });
});


//Buy album
album_routes.post('/buy/:id', auth.authenticate(), function (req, res) {
    var possibleErrors = [];
    if (!req.params.id || req.params.id == 0) {
        possibleErrors.push('id is required!');
    }
    if (possibleErrors.length > 0) {
        return handleErrorResponse(possibleErrors, res);
    }

    // TODO: Check if user alread bought this album ????

    Album.findOne({ _id: req.params.id }).populate('albumUserCreator').exec(function (err, album) {
        if (err || !album) {
            possibleErrors.push('Unable to find album for given id!');
        }

        var boughtAlbum = album.boughtBy.find(function (al) {
            return al.user.equals(req.user._id);
        });


        if (boughtAlbum)
            possibleErrors.push('Album has already been bought');

        if (req.user._id.equals(album.albumUserCreator._id))
            possibleErrors.push('Album is yours');

        if (possibleErrors.length > 0) {
            return handleErrorResponse(possibleErrors, res);
        }

        if (album.price == 0) {
            User.findOne({ _id: req.user._id }, function (err, currentUser) {
                if (err || !currentUser) {
                    possibleErrors.push('Unable to find user for given id!');
                }
                currentUser.bought.push({
                    album: req.params.id,
                    transaction: "FREE",
                    date: Date.now()
                });

                album.boughtBy.push({
                    user: req.user._id,
                    transaction: "FREE",
                    date: Date.now()
                });

                currentUser.save(function (errSaveUser) {
                    if (errSaveUser) {
                        possibleErrors.push('User not saved!');
                    }

                    album.totalPurchased++;
                    album.save(function (errAlbumSave) {
                        if (errAlbumSave) {
                            possibleErrors.push('Album not saved!');
                        }

                        if (possibleErrors.length == 0) {

                            var order = new Order();
                            order.price = 0;
                            order.from_stripe = "FREE";
                            order.to_stripe = album.albumUserCreator.content_creator_stripe_id;
                            order.transaction = "FREE";
                            order.date = Date.now();
                            order.user = req.user._id;
                            order.album = req.params.id;

                            order.save(function (errOrder) {
                                var act = new Activity();
                                act.message = "You have bought an album.";
                                act.activityType = "BOUGHT";
                                act.subject = {}; act.objecta = {};
                                act.subject.user = req.user;
                                act.objecta.user = req.user;
                                var subUsers = []; subUsers.push(req.user);
                                activityHelper.pushNewActivity(act, subUsers, function (resA) {
                                    res.status(200).jsonp({
                                        charge: { success: true }
                                    });
                                });

                                notificationsHelper.sendNotifications("You have new download", req.user.username + " has downloaded your free album " + album.name, album.albumUserCreator.fcmList);
                            });
                        } else {
                            return handleErrorResponse(possibleErrors, res);
                        }
                    });
                });
            });

        }

        var stripeToken = req.body.stripe_token;
        //var stripeToken = "tok_visa";
        stripe.charges.create({
            amount: album.price * 100, // price is in cents so we multiply by 100 since our price is in dollars
            currency: "usd",
            source: stripeToken, // 10% of the price
            receipt_email: req.user.email,
            destination: {
                amount: (album.price * 90).toFixed() - 64,
                account: album.albumUserCreator.content_creator_stripe_id,
            }
        }).
            then(function (charge) {

                // Bind album to user and via versa user to album
                User.findOne({ _id: req.user._id }, function (err, currentUser) {
                    if (err || !currentUser) {
                        possibleErrors.push('Unable to find user for given id!');
                    }

                    currentUser.bought.push({
                        album: req.params.id,
                        transaction: charge.id,
                        date: Date.now()
                    });

                    album.boughtBy.push({
                        user: req.user._id,
                        transaction: charge.id,
                        date: Date.now()
                    });

                    currentUser.save(function (errSaveUser) {
                        if (errSaveUser) {
                            possibleErrors.push('User not saved!');
                        }

                        album.totalPurchased++;
                        album.save(function (errAlbumSave) {
                            if (errAlbumSave) {
                                possibleErrors.push('Album not saved!');
                            }

                            if (possibleErrors.length == 0) {

                                var order = new Order();
                                order.price = album.price * 100;
                                order.from_stripe = stripeToken;
                                order.to_stripe = album.albumUserCreator.content_creator_stripe_id;
                                order.transaction = charge.id;
                                order.date = Date.now();
                                order.user = req.user._id;
                                order.album = req.params.id;

                                order.save(function (errOrder) {
                                    var act = new Activity();
                                    act.message = "You have bought a new album.";
                                    act.activityType = "BOUGHT";
                                    act.subject = {}; act.objecta = {};
                                    act.subject.user = req.user;
                                    act.objecta.user = req.user;
                                    var subUsers = []; subUsers.push(req.user);
                                    activityHelper.pushNewActivity(act, subUsers, function (resA) {
                                        res.status(200).jsonp({
                                            charge: charge
                                        });
                                    });
                                    notificationsHelper.sendNotifications("You have new purchase", req.user.username + " has purchased your album " + album.name, album.albumUserCreator.fcmList);
                                });
                            } else {
                                return handleErrorResponse(possibleErrors, res);
                            }
                        });
                    });
                });

            }).error(function (stripeError) {
                possibleErrors.push('stripe payment error!');
                return handleErrorResponse(possibleErrors, res);
            });

    });
});
album_routes.post('/v2/buy/:id', auth.authenticate(), function (req, res) {
    var possibleErrors = [];
    if (!req.params.id || req.params.id == 0) {
        possibleErrors.push('id is required!');
    }
    if (possibleErrors.length > 0) {
        return handleErrorResponse(possibleErrors, res);
    }

    // TODO: Check if user alread bought this album ????

    Album.findOne({ _id: req.params.id }).populate('albumUserCreator').exec(function (err, album) {
        if (err || !album) {
            possibleErrors.push('Unable to find album for given id!');
        }

        var boughtAlbum = album.boughtBy.find(function (al) {
            return al.user.equals(req.user._id);
        });


        if (boughtAlbum)
            possibleErrors.push('Album has already been bought');

        if (req.user._id.equals(album.albumUserCreator._id))
            possibleErrors.push('Album is yours');

        if (possibleErrors.length > 0) {
            return handleErrorResponse(possibleErrors, res);
        }

        if (album.price == 0) {
            User.findOne({ _id: req.user._id }, function (err, currentUser) {
                if (err || !currentUser) {
                    possibleErrors.push('Unable to find user for given id!');
                }
                currentUser.bought.push({
                    album: req.params.id,
                    transaction: "FREE",
                    date: Date.now()
                });

                album.boughtBy.push({
                    user: req.user._id,
                    transaction: "FREE",
                    date: Date.now()
                });

                currentUser.save(function (errSaveUser) {
                    if (errSaveUser) {
                        possibleErrors.push('User not saved!');
                    }

                    album.totalPurchased++;
                    album.save(function (errAlbumSave) {
                        if (errAlbumSave) {
                            possibleErrors.push('Album not saved!');
                        }

                        if (possibleErrors.length == 0) {

                            var order = new Order();
                            order.price = 0;
                            order.from_stripe = "FREE";
                            order.to_stripe = album.albumUserCreator.content_creator_stripe_id;
                            order.transaction = "FREE";
                            order.date = Date.now();
                            order.user = req.user._id;
                            order.album = req.params.id;

                            order.save(function (errOrder) {
                                var act = new Activity();
                                act.message = "You have bought an album.";
                                act.activityType = "BOUGHT";
                                act.subject = {}; act.objecta = {};
                                act.subject.user = req.user;
                                act.objecta.user = req.user;
                                var subUsers = []; subUsers.push(req.user);
                                activityHelper.pushNewActivity(act, subUsers, function (resA) {
                                    res.status(200).jsonp({
                                        charge: { success: true }
                                    });
                                });

                                notificationsHelper.sendNotifications("You have new download", req.user.username + " has downloaded your free album " + album.name, album.albumUserCreator.fcmList);
                            });
                        } else {
                            return handleErrorResponse(possibleErrors, res);
                        }
                    });
                });
            });

        }

        //var stripeToken = req.body.stripe_token;
        var charge=req.body.transaction;
        //var stripeToken = "tok_visa";
        if(charge) {

                // Bind album to user and via versa user to album
                User.findOne({ _id: req.user._id }, function (err, currentUser) {
                    if (err || !currentUser) {
                        possibleErrors.push('Unable to find user for given id!');
                    }

                    currentUser.bought.push({
                        album: req.params.id,
                        transaction: charge.id,
                        date: charge.date
                    });

                    album.boughtBy.push({
                        user: req.user._id,
                        transaction: charge.id,
                        date: charge.date
                    });

                    currentUser.save(function (errSaveUser) {
                        if (errSaveUser) {
                            possibleErrors.push('User not saved!');
                        }

                        album.totalPurchased++;
                        album.save(function (errAlbumSave) {
                            if (errAlbumSave) {
                                possibleErrors.push('Album not saved!');
                            }

                            if (possibleErrors.length == 0) {

                                var order = new Order();
                                order.price = album.price * 100;
                                //order.from_stripe = stripeToken;
                                //order.to_stripe = album.albumUserCreator.content_creator_stripe_id;
                                order.transaction = charge.id;
                                order.date = charge.date;
                                order.user = req.user._id;
                                order.album = req.params.id;

                                order.save(function (errOrder) {
                                    var act = new Activity();
                                    act.message = "You have bought a new album.";
                                    act.activityType = "BOUGHT";
                                    act.subject = {}; act.objecta = {};
                                    act.subject.user = req.user;
                                    act.objecta.user = req.user;
                                    var subUsers = []; subUsers.push(req.user);
                                    activityHelper.pushNewActivity(act, subUsers, function (resA) {
                                        res.status(200).jsonp({
                                            charge: charge
                                        });
                                    });
                                    notificationsHelper.sendNotifications("You have new purchase", req.user.username + " has purchased your album " + album.name, album.albumUserCreator.fcmList);
                                });
                            } else {
                                return handleErrorResponse(possibleErrors, res);
                            }
                        });
                    });
                });

            };

    });
});

//Rate album
album_routes.post('/rate/:id', auth.authenticate(), function (req, res) {
    var possibleErrors = [];
    if (!req.params.id || req.params.id == 0) {
        possibleErrors.push('id is required!');
    }
    if (!req.body.rating || req.body.rating == 0) {
        possibleErrors.push('body.rating is required!');
    }
    if (possibleErrors.length > 0) {
        return handleErrorResponse(possibleErrors, res);
    }

    Album.findOne({ _id: req.params.id }, function (err, album) {
        if (err || !album) {
            possibleErrors.push('Unable to find album for given id!');
        }

        function userAlreadyRatedAlbum(element, index, array) {
            return element.user.equals(req.user._id);
        }

        if (album.userRating.some(userAlreadyRatedAlbum)) {
            // Override user previous rating
            album.userRating.forEach(function (ur) {
                if (ur.user.equals(req.user._id)) {
                    ur.rating = req.body.rating;
                }
            });
        } else {
            // Add new user rating
            album.userRating.push({ user: req.user._id, rating: req.body.rating });
        }

        if (possibleErrors.length == 0) {
            album.save(function (err) {
                if (err) {
                    possibleErrors.push('Unable to add rating to album');
                }

                if (possibleErrors.length == 0) {
                    return res.status(201).jsonp({ status: 'success' });
                } else {
                    return handleErrorResponse(possibleErrors, res);
                }
            });
        } else {
            return handleErrorResponse(possibleErrors, res);
        }
    });
});


album_routes.get('/saved',
    auth.authenticate(),
    followingHelper.getFollowingUsers,
    albumHelper.getBoughtAlbums,
    albumHelper.getSubscribedAlbums,
    albumHelper.getFreeAlbums,
    function (req, res) {

        var possibleErrors = [];
        var skip = 0;
        var limit = 1000;
        var count = 0;
        var searcherId = mongoose.Types.ObjectId();

        if (req.query.size && Number(req.query.size) > 0) {
            limit = Number(req.query.size);
        }
        if (req.query.page && Number(req.query.page) >= 0) {
            skip = Number(req.query.page) * limit;
        }

        if (possibleErrors.length > 0) {
            return handleErrorResponse(possibleErrors, res);
        }

        if (req.user) {
            searcherId = req.user._id;
        }

        var boughtAlbums = req.boughtAlbums;
        var subscribedAlbums = req.subscribedAlbums;
        var freeAlbums = req.freeAlbums;

        var resultList = new Map();
        subscribedAlbums.forEach(function (element) {
            resultList.set(element._id.toString(), element);
        }, this);
        boughtAlbums.forEach(function (element) {
            if (!resultList.has(element._id.toString()))
                resultList.set(element._id.toString(), element);;
        }, this);

        freeAlbums.forEach(function (element) {
            if (!resultList.has(element._id.toString()))
                resultList.set(element._id.toString(), element);;
        }, this);

        var flattenedList = [];
        for (var [key, value] of resultList) {
            flattenedList.push(value);
        }
        // imageHelper.joinImagesCreatorsFollowing(flattenedList, req.following, function (albums_output) {
        //     res.status(200).jsonp(albums_output);
        // });
        res.status(200).jsonp(flattenedList);

    });

album_routes.get('/:id', auth.authenticate(), function (req, res) {
    var possibleErrors = [];


    if (possibleErrors.length > 0) {
        return handleErrorResponse(possibleErrors, res);
    }

    Album.findOne({ _id: req.params.id }).populate('albumUserCreator').exec(function (err, album) {
        if (err || !album) {
            possibleErrors.push('Unable to find album for given id!');
        }
        Image.find({ albumId: album._id }, function (errI, images) {
            var result = album.toObject();
            var imgResult = [];
            images.forEach(function (element) {
                imgResult.push(element.outputMappedResult());
            }, this);
            result.images = imgResult;

            if (possibleErrors.length == 0) {
                return res.status(200).jsonp({
                    album: result
                });
            } else {
                return handleErrorResponse(possibleErrors, res);
            }
        });
    });
});

album_routes.get('/v2/:id', auth.authenticate(), function (req, res) {
    var possibleErrors = [];


    if (possibleErrors.length > 0) {
        return handleErrorResponse(possibleErrors, res);
    }

    Album.findOne({ _id: req.params.id }).populate('albumUserCreator').populate({ path: 'content', populate: { path: 'image' } }).exec(function (err, album) {
        if (err || !album) {
            possibleErrors.push('Unable to find album for given id!');
        }
        if (possibleErrors.length == 0) {
            return res.status(200).jsonp({
                album: album
            });
        } else {
            return handleErrorResponse(possibleErrors, res);
        }
    });
});


module.exports = album_routes;